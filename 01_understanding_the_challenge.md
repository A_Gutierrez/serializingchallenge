start with files, pass them into a function. the function serves as a output to file in specified display format

___

function Reads the file. decode. presents the information

# DESIGN

call something like:

`> python.exe my_code input_file output_file  --input_format=json  --output_format='pprint'` 

- query available formats:
```
> python.exe -h
> usage: input output --input_format --output_format
> available input formats are: ['csv', 'json']
> available output formats are: ['pprint', 'html']
```

- to write down the testing files from a python shell:

  `>>> my_code.serialize(format='csv', data=something, path='path/to/dir')`

