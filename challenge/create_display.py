"""
this command line tool will take a file of serialized data and display it in the selected format

"""

import argparse
from challenge import _core


parser = argparse.ArgumentParser(description='Process a data file to generate a display.')
parser.add_argument("input_file")
parser.add_argument("output_file")
parser.add_argument("--input_format",
                    choices=_core.get_serializer_formats().keys(),
                    help='This flag is to determine how '
                         'the input file was serialized so '
                         'it can be read correctly. if not specified,'
                         ' json will be used')
parser.add_argument("--output_format",
                    choices=_core.get_display_formats().keys(),
                    help='This flag is to determine how '
                         'the output file is to be formatted. '
                         'If not specified, pprint_display will be used')
parser.add_argument("-p", help="If this flag is specified, the display "
                                    "is printed to screen after generating the "
                                    "display file",
                    action='store_true')


def main(source_file, dest_file, source_format, dest_format, print_to_screen):
    serializer = _core.get_serializer(source_format)
    display = _core.get_display(dest_format)

    with open(source_file, 'r') as read_file:
        data = serializer.deserialize(read_file.read())
    display.generate_display(data=data, file_path=dest_file)

    if print_to_screen:
        display.write_to_screen()


if __name__ == '__main__':
    arguments = parser.parse_args()
    input_file = arguments.input_file
    output_file = arguments.output_file
    input_format = arguments.input_format if arguments.input_format else 'json'
    output_format = arguments.output_format if arguments.output_format else 'pprint_display'

    main(source_file=input_file, dest_file=output_file,
         source_format=input_format, dest_format=output_format,
         print_to_screen=arguments.p)
