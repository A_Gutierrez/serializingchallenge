from pprint import pformat
import os
from typing import Optional


class _DisplayTemplate(object):
    """base class to use as a template to keep consistency
    dipslay method needs to be
    """
    name = None
    ext = str()

    def __init__(self):
        self.display_text = str()

    def generate_display(self, data, file_path=None):
        # type: (object, Optional[str]) -> None
        """Main function of the class, it should execute the logic needed to
        generate the display data, which gets saved in self.display_text.

        If the template is to write to a file, the method _write_to_file should be called,
        otherwise use _write_to_screen to display the text in a screen
        """
        raise NotImplementedError

    def _write_to_file(self, file_path):
        """This method writes to a file if the extension is correct"""
        if self.ext not in os.path.splitext(file_path)[-1]:
            raise IOError("Destination file does not have the correct extension")
        with open(file_path, 'w') as write_file:
            write_file.write(self.display_text)

    def write_to_screen(self):
        """This method writes to the screen/stdout"""
        print self.display_text


class PPrintDisplay(_DisplayTemplate):
    name = "pprint_display"
    ext = "txt"

    def generate_display(self, data, file_path):
        # type: (object, str) -> None
        self.display_text = self._generate_display_text(data)
        self._write_to_file(file_path)
        pass

    def _generate_display_text(self, data):
        return pformat(data, indent=4)


class HTMLDisplay(_DisplayTemplate):
    name = "html_display"
    ext = "html"

    def __init__(self):
        super(HTMLDisplay, self).__init__()
        self.html_ = '<!DOCTYPE html>\n' \
                     '<html lang="en">\n' \
                     '<body>\n' \
                     '{}' \
                     '</body>\n' \
                     '</html>'

        self.person_template = "<div>\n"\
                               "\t<h1>Person name: {name}</h1>\n"\
                               "\t<p>address: {address}</p>\n"\
                               "\t<p>phone number: {phone_number}</p>\n"\
                               "</div>\n"

    def generate_display(self, data, file_path):
        # type: (object, str) -> None
        self.display_text = self._generate_display_text(data)
        self._write_to_file(file_path)
        pass

    def _generate_display_text(self, data):
        people_html = []
        for person in data:
            people_html.append(self.person_template.format(**person))
        return self.html_.format("\n".join(people_html))
