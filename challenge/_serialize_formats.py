import json
import xml.etree.ElementTree as _ET


class _SerializerTemplate(object):
    """base class to use as a template to keep consistency
    the methods serialize and deserialize need to be overridden
    to extend the desired formats
    """
    name = None

    def serialize(self, data):
        # type: (object) -> None
        raise NotImplementedError

    def deserialize(self, data):
        """receives the information that was read from the file
        and converts it back into objects.

        If it fails to read the format, raise RuntimeError
        """
        # type: (object) -> object
        raise NotImplementedError


class JsonSerializer(_SerializerTemplate):
    name = "json"
    def serialize(self, data):
        return json.dumps(data)

    def deserialize(self, data):
        try:
            return json.loads(data)
        except ValueError:
            raise RuntimeError("Json could not be decoded. Source may be corrupted or have the wrong format")


class SimpleXMLSerializer(_SerializerTemplate):
    """serializes a flat dictionary of people into XML format"""
    name = "simple_xml"
    root = "<Root>\n\t{}\n</Root>"

    def serialize(self, data):
        # type: (dict) -> None
        xml_people = []
        for person in data:
            xml_people.append(self._serialize_person(person))
        return self.root.format('\n\t'.join(xml_people))

    def _serialize_person(self, person):
        name = person.pop("name")
        root = _ET.Element(name, attrib=person)
        return _ET.tostring(root)

    def deserialize(self, data):
        # type: (str) -> object
        try:
            people = list()
            root = _ET.fromstring(text=data)  # type: _ET.Element
            for person in list(root):
                people.append(self._deserialize_person(person))
            return people
        except _ET.ParseError:
            raise RuntimeError("Simple XML could not be decoded. Source may be corrupted or have the wrong format")

    def _deserialize_person(self, person):
        # type: (_ET.Element) -> dict
        deserialized = person.attrib
        deserialized["name"] = person.tag
        return deserialized
