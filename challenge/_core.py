from challenge import _serialize_formats, _display_formats


def _get_classes_from_module(module, template):
    """look through a module and return all subclasses of the specified template"""
    available_formats = dict()
    for name in dir(module):
        if name.startswith("_"):
            continue
        cls = getattr(module, name)
        format_name = getattr(cls, "name", None)
        try:
            if not issubclass(cls, template):
                continue
        except TypeError:
            # found something that was not a class
            continue
        if format_name:
            available_formats[format_name] = cls
    return available_formats


def get_serializer(format):
    # type: (str) -> _serialize_formats._SerializerTemplate
    """
    a simple factory to get the correct serializer based on a str type
    """
    available_formats = get_serializer_formats()
    serializer = available_formats.get(format)
    if not serializer:
        raise NotImplementedError("specified format does not exist")
    return serializer()


def get_serializer_formats():
    # type: () -> dict
    """
    get the serialize formats defined in our module, and map them by their
    pretty name
    :return: dict
    """
    available_formats = _get_classes_from_module(module=_serialize_formats,
                                                 template=_serialize_formats._SerializerTemplate)
    return available_formats


def get_display_formats():
    """
    get the display formats defined in our module, and map them by their
    pretty name
    :return: dict
    """
    available_formats = _get_classes_from_module(module=_display_formats,
                                                 template=_display_formats._DisplayTemplate)
    return available_formats


def get_display(format):
    # type: (str) -> _display_formats._DisplayTemplate
    """
        a simple factory to get the correct display based on a str type
    """
    available_formats = get_display_formats()
    display = available_formats.get(format)
    if not display:
        raise NotImplementedError("specified format does not exist")
    return display()
