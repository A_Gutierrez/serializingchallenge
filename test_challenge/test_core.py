import pytest
from challenge import _serialize_formats, _display_formats, _core


# TEST SERIALIZE FORMATS
serialize_format_names = ['json', 'simple_xml', 'fake']


@pytest.fixture()
def valid_serialize_formats():
    pretty_names = list()
    for name in dir(_serialize_formats):
        if name.startswith("_"):
            continue
        cls = getattr(_serialize_formats, name)

        try:
            if not issubclass(cls, _serialize_formats._SerializerTemplate):
                continue
        except TypeError:
            # found something that was not a class
            continue
        format_name = getattr(cls, "name", None)
        if format_name:
            pretty_names.append(format_name)

    return pretty_names


def test_query_serialize_formats_is_same_as_found_formats(valid_serialize_formats):
    """ the idea behind this test is that the query will have
     the same ammount of formats than the test."""
    for format_name, value in _core.get_serializer_formats().iteritems():
        if format_name in valid_serialize_formats:
            valid_serialize_formats.remove(format_name)

    assert len(valid_serialize_formats) == 0


@pytest.mark.parametrize("format", argvalues=serialize_format_names)
def test_serializer_factory_inputs(format, valid_serialize_formats):
    if format in valid_serialize_formats:
        serializer = _core.get_serializer(format)
        assert issubclass(type(serializer), _serialize_formats._SerializerTemplate)
        assert serializer.name == format
    else:
        with pytest.raises(NotImplementedError):
            serializer = _core.get_serializer(format)


# TEST DISPLAY FORMATS
display_format_names = ['pprint_display', 'fake']


@pytest.fixture()
def valid_display_formats():
    pretty_names = list()
    for name in dir(_display_formats):
        if name.startswith("_"):
            continue
        cls = getattr(_display_formats, name)

        try:
            if not issubclass(cls, _display_formats._DisplayTemplate):
                continue
        except TypeError:
            # found something that was not a class
            continue
        format_name = getattr(cls, "name", None)
        if format_name:
            pretty_names.append(format_name)

    return pretty_names


def test_query_display_formats_is_same_as_found_formats(valid_display_formats):
    """ the idea behind this test is that the query will have
     the same ammount of formats than the test."""
    for format_name, value in _core.get_display_formats().iteritems():
        if format_name in valid_display_formats:
            valid_display_formats.remove(format_name)

    assert len(valid_display_formats) == 0


@pytest.mark.parametrize("format", argvalues=display_format_names)
def test_display_factory_inputs(format, valid_display_formats):
    if format in valid_display_formats:
        display = _core.get_display(format)
        assert issubclass(type(display), _display_formats._DisplayTemplate)
        assert display.name == format
    else:
        with pytest.raises(NotImplementedError):
            display = _core.get_display(format)
