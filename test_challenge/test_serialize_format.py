import pytest
from challenge import _serialize_formats
import copy
import os

PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))      # path to the root of the project
DATA_EXAMPLE = [{"name": "John", "address": "123 abc place", "phone_number": "123 789 1245"},
                {"name": "Mary", "address": "456 abc place", "phone_number": "999 666 7789"},
                ]


def test_interface_raises_not_implemented():
    template = _serialize_formats._SerializerTemplate()

    with pytest.raises(NotImplementedError):
        template.serialize(data='something')

    with pytest.raises(NotImplementedError):
        template.deserialize(data='something')


# TEST OF CONCRETE IMPLEMENTATIONS

serializers = [_serialize_formats.JsonSerializer,
               _serialize_formats.SimpleXMLSerializer,
               ]


@pytest.fixture(scope="function", params=serializers)
def serializer(request):
    return request.param()


@pytest.mark.parametrize("data_ex", (DATA_EXAMPLE,))
def test_concrete_serialize(serializer, data_ex):
    # Here i'm having difficulties testing
    # this method in any significant way
    data = serializer.serialize(data=copy.deepcopy(data_ex))
    assert isinstance(data, basestring)


@pytest.mark.parametrize("data_ex", (DATA_EXAMPLE,))
def test_concrete_deserialized_data_same_as_input(serializer, data_ex):
    serialized_data = serializer.serialize(data=copy.deepcopy(data_ex))
    deserialized_data = serializer.deserialize(serialized_data)
    assert deserialized_data == data_ex


def test_serializer_has_name(serializer):
    """ The idea is to give a short name to the
    serializer that can be called in the factory"""
    assert serializer.name


testing_wrong_data = [(_serialize_formats.JsonSerializer, r"{base}\data\test_data_simple_xml.xml".format(base=PROJECT_ROOT)),
              (_serialize_formats.SimpleXMLSerializer, r"{base}\data\test_data_json.json".format(base=PROJECT_ROOT))
              ]
@pytest.mark.parametrize("serial, wrong_file", testing_wrong_data)
def test_wrong_format_raises_error(serial, wrong_file):
    with pytest.raises(RuntimeError), open(wrong_file, 'r') as file_read:
        serialize = serial()
        serialize.deserialize(file_read.read())
