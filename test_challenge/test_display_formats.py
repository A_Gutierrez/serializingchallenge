import pytest
from challenge import _display_formats
from pathlib2 import Path


DATA_EXAMPLE = [{"name": "John", "address": "123 abc place", "phone_number": "123 789 1245"},
                {"name": "Mary", "address": "456 abc place", "phone_number": "999 666 7789"},
                ]


def test_interface_raises_not_implemented():
    template = _display_formats._DisplayTemplate()
    with pytest.raises(NotImplementedError):
        template.generate_display(data='something', file_path="path/to/file.ext")


displays = [_display_formats.PPrintDisplay,
            _display_formats.HTMLDisplay,
            ]


@pytest.fixture(scope="function", params=displays)
def display(request):
    return request.param()

# TEST CONCRETE IMPLEMENTATIONS


@pytest.mark.parametrize("data", (DATA_EXAMPLE,))
def test_generate_display(display, data, tmp_path):
    """direct the result to a temp file, make sure
    that what is written is the same as what was generated.
    """
    temp_dir = tmp_path / "display"   # type: Path
    temp_dir.mkdir()
    temp_file_path = temp_dir / "display_result.{}".format(display.ext)

    display.generate_display(data=data, file_path=str(temp_file_path))
    with temp_file_path.open('r') as temp_file:
        assert temp_file.read() == display.display_text


@pytest.mark.parametrize("data", (DATA_EXAMPLE,))
def test_write_errors_when_wrong_extension_is_given(display, data, tmp_path):
    tmp_path = "dummy/path.fake"
    display.display_text = "hi!"

    with pytest.raises(IOError):
        display._write_to_file(tmp_path)
    pass

