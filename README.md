# Serializing Challenge

In Python or C++  write a command line tool which 
shows how you would take some sets of personal 
data  (name, address, phone number) and serialise
them/deserialise them in at least 2 formats,
and display it in at least 2 different ways 
(no need to use a GUI Framework - text output/HTML 
or any other human readable format is  fine).  
There is no need to support manual data entry - 
you could manually write a file in one of your 
chosen formats to give you your input test data.

Write it in such a way that it would be easy for a developer:

- to add support for additional storage formats
- to query a list of currently supported formats
- to supply an alternative reader/writer for one of the supported formats


This should ideally show Object-Oriented Design and 
Design Patterns Knowledge, we’re not looking for
 use of advanced Language constructs. Provide 
 reasonable Unit Test coverage.

# Environment Requirements:
- python 2.7
- pytest
- typing

# Known Issues:

The program is not currently verifying 
that the specified input format matches what 
the file was written as, and will therefore 
throw errors.  


# TODO:
- get feedback

# example usage:
to get the help message:
```
create_display.py -h
```


to use the default behaviour
```
create_display.py "\animal_logic_challenge\data\test_data_json.json" "\animal_logic_challenge\data\test_disp.txt"
```

to specify input or output flags
```
create_display.py "\animal_logic_challenge\data\test_data_json.json" "\animal_logic_challenge\data\test_disp.html" -p --input_format json --output_format html_display
```
