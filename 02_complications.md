# Complications

I'm going to write the complications here
 as I find them, so this file might work more 
 as a cronology of development than a 
 structured log of any sort.
  
-  Test_serialize:
    
    I am not sure how to make meaningful tests
    for something like the serialization in my 
    example. If there was more steps involved
    I could test those steps. 
    
- Decide on secondary formats to support:

    I decided to support Json since it's the 
    format I'm most familiar with, and would do 
    to get the structure right. Considered 
    supporting SQL, but I wouldn't know how to
    make that example without designing the
    whole database, so I discarded that though.
    I settled on XML since it seems simple enough.
    
- XML nested data:
    
    I don't want to spend a whole bunch of time
    perfecting an XML parser, so I'll just assume
    flat data. It can be adjusted later.
    
    
- serialize test:
 
    Decided that the serializer will always return
    a string, that way it complies with the
    request to make it human readable. Not perfect
    but I can live with it.
    
- XML serialization

    initial format is not useful, will need to 
    modify  it so that it's either better nested, 
    or all attributes are well handled
    (chose the second option, now we have the entire
    person in one section, and can
    be appended in a list-like fashion)
    
- testing the factory

    when creating the factory I only know of one way
    to get the classes in the file, so the test and 
    the production code ended up with essentially 
    the same code, even though it was written in 
    different work sessions.
    
- wrote the logic for a single data entry. 

    after re-reading the challenge description, it 
    became apparent that it was meant to handle 
    multiple people. Adjusting that now.
     
- display format basic template.
    
    I had to decide wether to subclass the serialize 
    template format or create it's own. I do believe 
    that they are distinct enough to merit their own 
    template. 

- XML, again.
    
    apparently I created an invalid xml file when 
    serializing I need to solve that. But First 
    I need to create the display which brings 
    me to the next complication
    
- HTML! 

    do I need to create a file and open a browser to
    display it? I will assume that I just need to
    create the file and not launch the subprocess.
    Otherwise it would mean that every display format
    would need to have it's own application launch,
    which is beyond the scope of the challenge.
    
     I have made one of the display objects. HTML 
     will be on pause until after I have finished 
     the rest of the structure, since that object
     will not interfere/influence the rest of the
     program.
     
- input data:
    
    there seems to be a conflict of interests in 
    the description of the challenge:
    > take some sets of personal 
        data  (name, address, phone number) and serialise
        them/deserialise(...)
        
    vs 
    
    > There is no need to support manual data entry
    
    So, in the interest of time, I'm going to assume that
    the tool has another part where the serializer is 
    being used to create the files that my part is going
    to read, and that task just happened to fall on
    another developer (aka: I don't want to write the 
    command line tool to take the data manually, however 
    the data sets i have were created using this tool 
    from a python shell). 


- args parse

    how do I test this thing?!